
from DDPG2 import Agent
import gym
import numpy as np 
import matplotlib.pyplot as plt
import os


if __name__ == '__main__':
    os.environ['CUDA_DEVICE_ORDER']= 'PCI_BUS_ID'
    os.environ['CUDA_VISIBLE_DEVICES']= '0'

    env = gym.make('BipedalWalker-v2')

    agent = Agent(alpha = 0.0001, beta=0.001, input_dims=[24], tau=0.05, env=env,n_actions=4,max_size=100000)
    
    score_history=[]
    c=0
    for i in range(5000):
        obs = env.reset()
        done = False
        score = 0
        c= 0
        while not done:
            c+= 1.
            act = agent.choose_action(obs)
            new_state, reward, done, info = env.step(act)
            agent.remember(obs,act,reward,new_state,int(done))
            agent.learn()
            score += reward
            obs = new_state
            env.render()
            if c%20==0:
                print(reward)
        score_history.append(score)
        
        print('episode', i, 'score %.2f' %score,
            'trailing 100 games avg %.2f' %np.mean(score_history[-100:]))

        if i%25==0:
            #agent.save_models()
            
            filename='bipedal.png'
            plt.xlabel('Time')
            plt.ylabel('Rewards')
            plt.plot(score_history)
            plt.show()
            plt.savefig(filename)